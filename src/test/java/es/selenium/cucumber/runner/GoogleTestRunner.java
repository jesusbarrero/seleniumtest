package es.selenium.cucumber.runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src/test/resources/features/GoogleTest.feature",
		plugin = { "pretty", "html:target/cucumber", "json:target/cucumber/GoogleTest.json" },
		glue = {"es.selenium.cucumber.stepDef"}
		)

public class GoogleTestRunner {

}
