package es.selenium.cucumber.stepDef;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GoogleStepDef {
	
	private WebDriver driver;
	//WebDriver driver = new ChromeDriver();
	private WebElement element;
	private String searchKey;
	
	@Given("^Accedemos a google$")
	public void accedemos_a_google() {
		try {
			initRemoteDriver();
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		driver.get("http://www.google.es");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@When("^Se realiza la busqueda de (.*?)$")
	public void se_realiza_una_busqueda(String searchKey){ 
		element = driver.findElement(By.name("q"));
		
		this.searchKey = searchKey;
		
		element.sendKeys(this.searchKey);
		element.submit();
	}

	@Then("^Se muestran los resultados correctamente$")
	public void se_muestran_los_resultados_correctamente(){
		String url = driver.getCurrentUrl();
		
		this.searchKey = this.searchKey.replace(" ","+");   //Sustituimos los espacios por +
		assert url.contains(this.searchKey);
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@After
	public void quitDriver() {
		
		driver.close();
	}
	
	//PRIVATE METHODS
	
	private void initRemoteDriver() throws MalformedURLException {
		this.driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.chrome());
	}
	

}
